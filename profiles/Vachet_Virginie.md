# Vachet Virginie

### Location

Lyon, France

### Academics

[Simplon Lyon](https://www.simplonlyon.fr/)

### Interests

- Hacking
- History
- Dev
- Soccer

### Development

- Full stack developer


### Profile Link

[Vachet Virginie](https://github.com/VachetVirginie)
